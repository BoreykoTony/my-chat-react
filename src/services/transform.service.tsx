import {_message} from '../POJOs/_message';

class TransformService {
    transform(message: _message): _message {
        return {
            id: message.id,
            userId: message.userId,
            avatar: message.avatar,
            user: message.user,
            text: message.text,
            createdAt: new Date(message.createdAt),
            editedAt: message.editedAt ? new Date(message.editedAt) : undefined,
            liked: false
        }

    }

    transformAll(messages: _message[]): _message[] {
        return messages.map(message => this.transform(message))
    }
}

export const transformService = new TransformService();
