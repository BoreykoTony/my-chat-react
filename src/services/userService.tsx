import {v4 as uuidv4} from 'uuid';
import {_user} from '../POJOs/_user';

const myId = uuidv4();

export const getUser = (): _user => {
    return {
        id: myId,
        username: "professorik"
    }
}