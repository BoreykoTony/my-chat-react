export const getFormattedDate = (date: Date): string => {
    const now = new Date();
    if (date.getDay() === now.getDay() && date.getMonth() === now.getMonth() &&
        date.getFullYear() === now.getFullYear()) {
        return "Today";
    }
    return new Intl.DateTimeFormat('en-GB', {
        weekday: 'long',
        day: 'numeric',
        month: 'long'
    }).format(date)
}
