import React, {Component} from 'react';
import {Button, Form} from 'react-bootstrap';
import './styles/MessageInput.css'

interface Props {
    onAddMessage: (message: string) => void,
    onEditMessage: (message: string) => void,
    text: string
    isEditing: boolean
}

interface State {
    text: string
    touched: boolean
    isWarning: boolean
}

export class MessageInput extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {text: '', touched: false, isWarning: false}
    }

    handleChange = (e: any) => {
        if (!this.state.touched) {
            this.setState({isWarning: this.state.isWarning, text: this.state.text, touched: true})
        }
        this.setState({isWarning: this.state.isWarning, touched: true, text: e.target.value})
    }

    handleSubmit = () => {
        const {text} = this.state
        if (text.trim() === "") {
            this.setState({isWarning: true, touched: this.state.touched, text: this.state.text})
        }else {
            if (this.props.isEditing) {
                this.props.onEditMessage(text);
            } else {
                this.props.onAddMessage(text);
            }
            this.setState({text: '', touched: false, isWarning: false})
        }
    }

    render() {
        const {isEditing, text: textFromProps} = this.props
        const {touched, text: textFromState, isWarning} = this.state
        return (
            <div className="message-input">
                <Form.Control
                    value={isEditing && !touched ? textFromProps : textFromState}
                    onChange={this.handleChange} className="message-input-text" type="text"
                    placeholder={isWarning ? "Cannot be empty" : "Message"} size="sm"/>
                <Button className="message-input-button" onClick={this.handleSubmit} type="submit" size="sm">
                    {isEditing ? 'Edit' : 'Send'}
                </Button>
            </div>
        )
    }
}
