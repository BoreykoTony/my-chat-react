import React, {Component} from 'react';
import {v4 as uuidv4} from 'uuid';
import {Header} from './Header';
import {MessageList} from './MessageList';
import {MessageInput} from './MessageInput';
import {ApiService} from '../services/api.service';
import {_message} from '../POJOs/_message';
import {transformService} from '../services/transform.service';
import {Preloader} from './Preloader';
import {getUser} from '../services/userService';
import {_user} from '../POJOs/_user';
import logo from '../resources/letter.png'

import './styles/Chat.css'

interface ChatProps {
    url: string
}

interface ChatState {
    messages: _message[],
    isEditing: boolean,
    editingMessage: string,
    editingMessageId: string,
    loading: boolean
}

export default class Chat extends Component<ChatProps, ChatState> {
    apiService: ApiService;

    constructor(props: ChatProps) {
        super(props);

        this.apiService = new ApiService(props.url)
        this.state = {
            messages: [],
            isEditing: false,
            editingMessage: '',
            editingMessageId: '',
            loading: true
        }
    }

    componentDidMount(): void {
        this.renderMessages();
    }

    renderMessages = async () => {
        try {
            const messages: _message[] = transformService.transformAll(await this.apiService.fetchMessages());

            this.setState({
                editingMessage: this.state.editingMessage, editingMessageId: this.state.editingMessageId,
                isEditing: this.state.isEditing,
                messages: messages.sort((message1, message2) => message1.createdAt > message2.createdAt ? 1 : -1),
                loading: false
            });
        } catch (err) {

        }
    }

    addMessage = (message: string): void => {
        const messages: _message[] = [...this.state.messages];

        const user: _user = getUser();

        messages.push({
            id: uuidv4(),
            userId: user.id,
            avatar: "https://unsplash.it/36/36?gravity=center",
            user: user.username,
            text: message,
            createdAt: new Date(),
            liked: false
        });
        this.setState({loading: this.state.loading, editingMessage: this.state.editingMessage, editingMessageId: this.state.editingMessageId,
            isEditing: this.state.isEditing, messages})
    }

    toggleLike = (id: string): void => {
        let messages: _message[] = [...this.state.messages];

        const index = messages.findIndex(message => message.id === id);
        if (messages[index]) {
            messages[index].liked = !messages[index]?.liked;
            this.setState({loading: this.state.loading, editingMessage: this.state.editingMessage, editingMessageId: this.state.editingMessageId,
                isEditing: this.state.isEditing, messages})
        }
    }

    deleteMessage = (id: string): void => {
        let messages: _message[] = [...this.state.messages];

        const index = messages.findIndex(message => message.id === id);
        if (messages[index]) {
            messages.splice(index, 1)
            this.setState({loading: this.state.loading, editingMessage: this.state.editingMessage, editingMessageId: this.state.editingMessageId,
                isEditing: this.state.isEditing, messages})
        }
    }

    editMessage = (text: string): void => {
        let messages: _message[] = [...this.state.messages];

        const index = messages.findIndex(message => message.id === this.state.editingMessageId);
        if (messages[index]) {
            messages[index].text = text;
            messages[index].editedAt = new Date();
            this.setState({loading: this.state.loading, messages,
                editingMessageId: '', editingMessage: '', isEditing: false})
        }
    }

    invokeEditionMessage = (id: string, text: string): void => {this.setState({
        loading: this.state.loading, editingMessageId: id, editingMessage: text, isEditing: true,
    messages: this.state.messages})}

    render() {
        if (this.state.loading) {
            return <Preloader/>
        }

        const {messages, editingMessage, isEditing} = this.state

        const numberUniqueParticipants = countUniqueParticipants(messages)

        const lastMessageDate: Date = getLastMessageDate(messages)

        return (
            <div className="chat container">
                <img src={logo} className="logo" alt="logo"/>
                <Header chatName={'My Chat'} participantsCount={numberUniqueParticipants}
                        messagesCount={messages.length}
                        lastMessageDate={`${lastMessageDate.toLocaleDateString()}
                        ${(lastMessageDate.getHours()>9?'':'0')+lastMessageDate.getHours()}:${
                            (lastMessageDate.getMinutes()>9?'':'0')+lastMessageDate.getMinutes()}`}/>
                <MessageList onLike={this.toggleLike} onDelete={this.deleteMessage} onEdit={this.invokeEditionMessage}
                             messages={messages}/>
                <MessageInput onAddMessage={this.addMessage} onEditMessage={this.editMessage}
                              text={editingMessage} isEditing={isEditing}/>
                <footer>@Copyleft 2021</footer>
            </div>
        );

    }

}

const countUniqueParticipants = (messages: _message[]): number => {
    const participants: string[] = []

    messages.forEach(message => {
        if (!participants.includes(message['userId'])) {
            participants.push(message['userId'])
        }
    })

    return participants.length;
}

const getLastMessageDate = (messages: _message[]): Date => {
    return messages[messages.length - 1]['createdAt'];
}
