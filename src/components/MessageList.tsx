import React from 'react';
import {_message} from '../POJOs/_message';
import {Message} from './Message';
import {OwnMessage} from './OwnMessage';
import {getFormattedDate} from '../services/dateService';
import {getUser} from '../services/userService';
import './styles/MessageList.css'

interface MessageListProps {
    messages: _message[]
    onLike: (id: string) => void
    onEdit: (id: string, text: string) => void
    onDelete: (id: string) => void
}

export const MessageList: React.FC<MessageListProps> = ({messages, onLike, onEdit, onDelete}) => {
    const renderOutput = prepareMessagesForRender(messages, onLike, onEdit, onDelete)
    return (<div className="message-list">{renderOutput}</div>)
}

const prepareMessagesForRender = (messages: _message[], onDeleteHandler: (id: string) => void,
                                  onEditHandler: (id: string, text: string) => void,
                                  onLikeHandler: (id: string) => void): JSX.Element[] => {
    const messagesByDayMap = convertMessageArrayToMap(messages);
    return convertMessagesByDayMapToJsx(messagesByDayMap, onDeleteHandler, onEditHandler, onLikeHandler);
}

const convertMessageArrayToMap = (messages: _message[]): Map<string, _message[]> => {
    let messagesByDay: Map<string, _message[]> = new Map<string, _message[]>();

    messages.forEach(message => {
        const day = getFormattedDate(message.createdAt)
        if (messagesByDay.has(day)) {
            let messages = messagesByDay.get(day);
            if (messages) {
                messages.push(message)
                messagesByDay.set(day, messages)
                return
            }
        }
        messagesByDay.set(day, Array.of(message))
    })
    return messagesByDay;
}

const convertMessagesByDayMapToJsx = (map: Map<string, _message[]>,
                                      onLikeHandler: (id: string) => void,
                                      onEditHandler: (id: string, text: string) => void,
                                      onDeleteHandler: (id: string) => void): JSX.Element[] => {
    const renderOutput: JSX.Element[] = [];
    map.forEach((messages, day) => {
        renderOutput.push(
            <div key={day} className="message-divider">{day}</div>
        )
        messages.forEach(message => {
            let h = message.createdAt.getHours();
            let m = message.createdAt.getMinutes();
            if (message.userId === getUser().id) {
                renderOutput.push(<OwnMessage key={message.id}
                                              text={message.text}
                                              createdAt={(h>9 ? '' : '0') + h + ":" + (m>9 ? '' : '0') + m}
                                              onDelete={() => onDeleteHandler(message.id)}
                                              onEdit={() => onEditHandler(message.id, message.text)}/>)
            } else {
                renderOutput.push(<Message key={message.id}
                                            id={message.id} userId={message.userId} avatar={message.avatar}
                                            user={message.user}
                                            text={message.text}
                                            createdAt={(h>9 ? '' : '0') + h + ":" + (m>9 ? '' : '0') + m}
                                            liked={message.liked}
                                            onLike={() => onLikeHandler(message.id)}/>)
            }
        })
    })
    return renderOutput
}
