import React from 'react';
import './styles/Preloader.css'

interface PreloaderProps {
}

export const Preloader: React.FC<PreloaderProps> = () => {
    return (
        <div className="preloader">
            <div className="circle-1">
                <div className="circle-2">
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    )
}
